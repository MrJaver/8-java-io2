package com.example.task01;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class Task01Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(extractSoundName(new File("src/main/resources/3724.mp3")));

    }
    public static String extractSoundName(File file) throws IOException, InterruptedException {
        if(file==null){
            throw new IllegalArgumentException("Argument is null");
        }
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("src/main/resources/ffprobe.exe", "-v", "error", "-of", "flat", "-show_format", file.getName())
                .directory(new File("src/main/resources/"))
                .redirectOutput(ProcessBuilder.Redirect.PIPE)
                .redirectInput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.INHERIT);
        Process process = processBuilder.start();


        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
            String line;
            while((line=reader.readLine())!=null)
            {
                if(line.contains("format.tags.title"))
                    return line.substring(line.indexOf("=")+2,line.length()-1);
            }
        }
        int exit=process.waitFor();

        return null;
    }
}
